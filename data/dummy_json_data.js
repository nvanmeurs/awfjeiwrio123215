dummy_json_data = [
	{
	"asset":"Mars",
	"pu":"Mars & Olympus",
	"altitude":28.1010000,
	"longitude":-89.1322000,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"Olympus",
	"pu":"Mars & Olympus",
	"altitude":28.0961000,
	"longitude":-89.1435000,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Brutus",
	"pu":"Mature Dev (Brutus,Auger,Ursa)",
	"altitude":27.4780000,
	"longitude":-90.3890000,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"Auger",
	"pu":"Mature Dev (Brutus,Auger,Ursa)",
	"altitude":27.3276000,
	"longitude":-92.2660000,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"Ursa",
	"pu":"Mature Dev (Brutus,Auger,Ursa)",
	"altitude":28.0938000,
	"longitude":-89.0633000,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"Perdido",
	"pu":"Perdido",
	"altitude":26.0778000,
	"longitude":-94.5383000,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Appo",
	"pu":"Growth (Appo,Vito,Stones)",
	"altitude":27.5000000,
	"longitude":-88.7500000,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"Vito",
	"pu":"Growth (Appo,Vito,Stones)",
	"altitude":27.5000000,
	"longitude":-88.7500000,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"Stones",
	"pu":"Growth (Appo,Vito,Stones)",
	"altitude":27.5000000,
	"longitude":-88.7500000,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"Late Life",
	"pu":"Late Life",
	"altitude":29.5555500,
	"longitude":-87.5555000,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Nakika",
	"pu":"NOV (Nakika,Tonga,Sable)",
	"altitude":28.5500000,
	"longitude":-91.0000000,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"Tonga",
	"pu":"NOV (Nakika,Tonga,Sable)",
	"altitude":28.5500000,
	"longitude":-91.0000000,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"Sable",
	"pu":"NOV (Nakika,Tonga,Sable)",
	"altitude":28.5500000,
	"longitude":-91.0000000,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"BC10",
	"pu":"Brazil BC10, BJSA, BMS-54",
	"altitude":-12.1238000,
	"longitude":-39.4433000,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"BJSA",
	"pu":"Brazil BC10, BJSA, BMS-54",
	"altitude":-22.3910000,
	"longitude":-40.2570000,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"BMS-54",
	"pu":"Brazil BC10, BJSA, BMS-54",
	"altitude":-23.4400000,
	"longitude":-41.5600000,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"Brazil Libra",
	"pu":"Brazil Libra",
	"altitude":-24.4100000,
	"longitude":-43.7600000,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"Venezuela",
	"pu":"Venezuela",
	"altitude":10.7549070,
	"longitude":-65.7540590,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Albian Sands - Overall",
	"pu":"AOSP",
	"altitude":56.7307500,
	"longitude":-111.4525995,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"Albian Sands - Muskeg River",
	"pu":"AOSP",
	"altitude":57.2508097,
	"longitude":-111.5214630,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"Albian Sands - Jack Pine Mine",
	"pu":"AOSP",
	"altitude":57.2547420,
	"longitude":-111.3543720,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"Scotford Upgrader",
	"pu":"AOSP",
	"altitude":53.7228876,
	"longitude":-113.2091990,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Cliffdale battery",
	"pu":"In Situ",
	"altitude":56.1823500,
	"longitude":-116.3249300,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"Peace River complex",
	"pu":"In Situ",
	"altitude":56.2257000,
	"longitude":-116.4732900,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"AERA",
	"pu":"AERA",
	"altitude":35.3716640,
	"longitude":-119.0296730,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"Appalachia",
	"pu":"Appalachia",
	"altitude":41.6850000,
	"longitude":-79.1900000,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Magnolia",
	"pu":"Magnolia",
	"altitude":32.0600000,
	"longitude":-93.4400000,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"Pinedale",
	"pu":"Pinedale",
	"altitude":42.8654900,
	"longitude":-109.8707000,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"Permian",
	"pu":"Permian",
	"altitude":31.8874800,
	"longitude":-102.3579500,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"Groundbirch, Gundy N & S",
	"pu":"Groundbirch, Gundy N & S",
	"altitude":55.6000000,
	"longitude":-120.0000000,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Deep Basin",
	"pu":"Deep Basin, Fox Creek",
	"altitude":54.3973370,
	"longitude":-116.7893300,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"Fox Creek",
	"pu":"Deep Basin, Fox Creek",
	"altitude":54.3973370,
	"longitude":-116.7893300,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"Foothills",
	"pu":"Foothills",
	"altitude":53.0669600,
	"longitude":-116.7824300,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"Gundy NE",
	"pu":"Gundy NE, Attachie",
	"altitude":55.7800000,
	"longitude":-120.9200000,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Attachie",
	"pu":"Gundy NE, Attachie",
	"altitude":55.7800000,
	"longitude":-120.9200000,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"Argentina",
	"pu":"Argentina/Colombia",
	"altitude":-33.9200000,
	"longitude":-64.3900000,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"Colombia",
	"pu":"Argentina/Colombia",
	"altitude":-33.9200000,
	"longitude":-64.3900000,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"SENA",
	"pu":"SENA",
	"altitude":29.8171700,
	"longitude":-95.4012900,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"DW Exploration NA",
	"pu":"DW Exploration NA",
	"altitude":24.2886120,
	"longitude":-89.4474600,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"DW Exploration SA",
	"pu":"DW Exploration SA",
	"altitude":8.0000000,
	"longitude":-55.0000000,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"LNG CA Value Chain",
	"pu":"LNG CA Value Chain",
	"altitude":53.9528900,
	"longitude":-128.7225900,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"Elba LNG",
	"pu":"Elba LNG",
	"altitude":32.0900000,
	"longitude":-81.0000000,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Atlantic LNG",
	"pu":"Atlantic & Peru LNG",
	"altitude":10.4072461,
	"longitude":-61.2955452,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"Peru LNG",
	"pu":"Atlantic & Peru LNG",
	"altitude":-13.3399633,
	"longitude":-76.2281782,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"Rock River",
	"pu":"Wind",
	"altitude":41.5946894,
	"longitude":-106.2083459,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"WhiteWater Hill",
	"pu":"Wind",
	"altitude":33.9239037,
	"longitude":-116.6169585,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Cabazon",
	"pu":"Wind",
	"altitude":33.9137718,
	"longitude":-116.7764561,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"Brazos",
	"pu":"Wind",
	"altitude":32.8819722,
	"longitude":-101.1496065,
	"kpi1":"grey",
	"kpi2":"green"
	},
	{
	"asset":"Colorado Green",
	"pu":"Wind",
	"altitude":37.7043990,
	"longitude":-102.6099010,
	"kpi1":"green",
	"kpi2":"yellow"
	},
	{
	"asset":"Mount Storm",
	"pu":"Wind",
	"altitude":39.2752290,
	"longitude":-79.2406242,
	"kpi1":"yellow",
	"kpi2":"red"
	},
	{
	"asset":"Top of Iowa",
	"pu":"Wind",
	"altitude":43.4417684,
	"longitude":-93.3541597,
	"kpi1":"red",
	"kpi2":"grey"
	},
	{
	"asset":"White Deer",
	"pu":"Wind",
	"altitude":35.5098223,
	"longitude":-101.0873761,
	"kpi1":"grey",
	"kpi2":"red"
	},
	{
	"asset":"Alaska",
	"pu":"Alaska",
	"altitude":71.1549950,
	"longitude":-163.1140499,
	"kpi1":"red",
	"kpi2":"red"
	}
]