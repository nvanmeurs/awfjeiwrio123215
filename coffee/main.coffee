class Map
    # Constant containing valid basemap options.
    @BASEMAP_NAMES: [
        "ShadedRelief"
        "Oceans"
        "Gray"
        "DarkGray"
        "Imagery"
        "Terrain"
    ]

    # Constant containing valid draw mode options.
    @DRAW_MODES: [
        "kpi1"
        "kpi2"
    ]

    # Initialize the map.
    constructor: (@elementId, options) ->
        {@center, @zoom, @basemapName,
        @markerData, @drawMode, @popupMaxWidth, @minZoom} = options

        @setDefaultValues()
        @initializeMap()
        @setBaseMap @basemapName
        @drawMarkers @drawMode if @markerData

    # Set undefined options to their default values.
    setDefaultValues: ->
        @hasLabels = false
        @basemapName = @constructor.BASEMAP_NAMES[0]
        @drawMode = @constructor.DRAW_MODES[0]
        @center = [31.12819929911196, -89.82421875]
        @zoom = 3
        @minZoom = 3
        @popupMaxWidth = 600
        @markersLayer = L.layerGroup()

    # Initialize and bootstrap the map with the desired settings.
    initializeMap: ->
        @map = L.map @elementId,
            center: @center
            zoom: @zoom
            minZoom: @minZoom

    # Add or change the current basemap using a basemap name.
    setBaseMap: (basemapName) ->
        for layer in [@basemapLayer, @basemapLabelsLayer]
            @map.removeLayer layer if layer

        @basemapLayer = L.esri.basemapLayer basemapName

        @basemapLabelsLayer = if basemapName in @constructor.BASEMAP_NAMES then L.esri.basemapLayer "#{basemapName}Labels" else null

        for layer in [@basemapLayer, @basemapLabelsLayer]
            @map.addLayer layer if layer

    # Determine if the draw mode of the markers should be changed.
    shouldChangeDrawMode: (drawMode) ->
        return drawMode isnt @drawMode

    # Determine if labels should be created for the markers.
    shouldCreateLabels: (zoomLevel, hasLabels) ->
        return zoomLevel >= 5 and not hasLabels

    # Determine if labels should be removed from the markers.
    shouldRemoveLabels: (zoomLevel, hasLabels) ->
        return zoomLevel < 5 and hasLabels

    # Determine if the labels have to be either removed or created.
    shouldChangeLabels: (zoomLevel, hasLabels) ->
        return @shouldCreateLabels(zoomLevel, hasLabels) or @shouldRemoveLabels(zoomLevel, hasLabels)

    # Determine if the markers have to be redrawn.
    shouldRedrawMarkers: (drawMode, zoomLevel, hasLabels) ->
        return @shouldChangeDrawMode(drawMode) or @shouldChangeLabels(zoomLevel, hasLabels)

    ###
     Create an instance of Leaflet's Marker class
     with the correct settings based on a marker object.
    ###
    createMarkerInstance: (markerObject, drawMode, bindLabels, bindPopups) ->
        markerInstance = L.marker [markerObject.altitude, markerObject.longitude],
                icon: L.icon
                    iconUrl: "img/#{markerObject[drawMode]}_pin.png"
                    size: [48, 48]
                    iconAnchor: [32, 58]
                    popupAnchor: [0, -44]

        @bindLabelToMarker markerInstance, markerObject if bindLabels
        @bindPopupToMarker markerInstance, markerObject if bindPopups
        return markerInstance

    ###
     Add the markers to their own markers layer
     and add the layer to the map.
    ###
    drawMarkers: (drawMode, options) ->
        options ?= {}
        {bindLabels, bindPopups} = options
        bindLabels ?= off
        bindPopups ?= on
        @hasLabels = bindLabels
        @drawMode = drawMode

        @markersLayer.addLayer @createMarkerInstance(obj, drawMode, bindLabels, bindPopups) for obj in @markerData
        @markersLayer.addTo @map

    ###
     Determine if the markers on the map should be redrawn
     and re-draw the markers with the desired settings.
    ###
    redrawMarkers: (drawMode) ->
        drawMode ?= @drawMode
        zoomLevel = @map.getZoom()

        return if not @shouldRedrawMarkers drawMode, zoomLevel, @hasLabels

        @markersLayer.clearLayers()
        @hasLabels = false

        @drawMarkers drawMode,
            bindLabels: @shouldCreateLabels(zoomLevel, @hasLabels)

    ###
     Create an instance of Leaflet's Popup class
     with the desired settings and bind it to an instance of
     Leaflet's Marker class.
    ###
    bindPopupToMarker: (markerInstance, markerObject) ->
        # TODO: Add actual algorithm to calculate the statistics.
        randomNumber = (max) -> Math.floor (Math.random() * max) + 1
        actuals = randomNumber 200
        target = randomNumber 1000
        variance = target-actuals
        variance_percentage = Math.round (variance/target) * 100

        # TODO: Render HTML using node elements instead of a blockstring.
        popupContent = """
                       <table>
                           <tr>
                               <td>Asset</td><td>:</td><td>#{markerObject.asset}</td>
                               <td></td>
                                <td>YTD</td><td>:</td><td>July 2014</td>
                           </tr>

                           <tr>
                               <td>PU</td><td>:</td><td>#{markerObject.pu}</td>
                               <td></td>
                               <td>Actuals</td><td>:</td><td>#{actuals}</td>
                           </tr>

                           <tr>
                               <td>LOB</td><td>:</td><td>Deep Water</td>
                               <td></td>
                               <td>Target</td><td>:</td><td>#{target}</td>
                           </tr>

                           <tr>
                               <td>KPI</td><td>:</td><td>#{@drawMode}</td>
                               <td></td>
                               <td>Variance</td><td>:</td><td>#{variance} (#{variance_percentage}%)</td>
                           </tr>

                       </table>
                       <img src="img/dummy_stats.png" width="300" height="200" alt="Dummy statistical picture" />
                       """

        popup = L.popup(maxWidth: @popupMaxWidth).setContent popupContent
        markerInstance.bindPopup popup

    ###
     Create an instance of Leaflet's Label plugin class
     with the desired settings and bind it to an instance of
     Leaflet's Marker class.
    ###
    bindLabelToMarker: (markerInstance, markerObject) ->
        markerInstance.bindLabel markerObject.asset,
            noHide: on
            direction: 'auto'

# Create an option element based on a value.
createOptionElement = (option) ->
    optionElement = document.createElement "option"
    optionElement.text = optionElement.value = option
    return optionElement

# Populate a select element with given options.
populateSelectWithOptions = (selectElement, options) ->
    selectElement.appendChild createOptionElement(option) for option in options

# Get the basemapSelector element.
basemapSelector = document.getElementById "basemapSelector"

# Populate the basemap selector with valid basemap names.
populateSelectWithOptions basemapSelector, Map.BASEMAP_NAMES

# Change the basemap layer of the map when the user selects another basemap.
basemapSelector.addEventListener "change", -> mapInstance.setBaseMap basemapSelector.options[basemapSelector.selectedIndex].value

# Get the drawModeSelector element.
drawModeSelector = document.getElementById "drawModeSelector"

# Populate the draw mode selector with valid draw modes.
populateSelectWithOptions drawModeSelector, Map.DRAW_MODES

# Change the draw mode of the markers on the map when the user selects another draw mode.
drawModeSelector.addEventListener "change", -> mapInstance.redrawMarkers drawModeSelector.options[drawModeSelector.selectedIndex].value

# Initialize and render the map.
mapInstance = new Map "map",
    markerData: dummy_json_data

# Add labels to the markers when the user has zoomed in far enough.
mapInstance.map.on 'zoomend', (e) -> mapInstance.redrawMarkers()